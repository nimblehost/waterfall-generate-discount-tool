# Waterfall Generate Discount Tool #

This PHP script will generate a unique discount code based on a customer's Waterfall serial number. By default the code created is a 30% off one-time use discount, which expires December 31, 2015.

Currently the script supports [Cartloom](http://cartloom.com) only. If you use a different shopping cart provider which has an API that can be used to remotely create discount code, please get in touch so we can look into adding support for it in this script.

### How do I use it? ###

1. Download the script (or clone the repo) and specify your API key for the `$apiKey` variable. If you are customizing this for use with a different shopping cart provider, you'll also need to change the `$url` variable to match your service, and probably also need to tweak the various `$data` parameters for the discount code being generated.
2. Publish this file to your server, and [notify us](https://www.nimblehost.com/support/contact) of the exact URL to the file.
3. Each time a copy of Waterfall is purchased, the NimbleHost server will contact the URL you've specified. This tool will then generate the discount for you, and return a `success` or `failure` message. We'll include the discount code in the email sent to each customer, along with a link to your website where they can purchase your Waterfall compatible products.