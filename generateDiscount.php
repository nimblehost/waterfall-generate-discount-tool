<?php

function log_line($text) {
	// You can change the logfile name here
	$logfile = 'valid-waterfall-discount-codes_'. date("Y-m") .'.log';
	$log_line = date("D M j Y G:i:s T").' '.$text."\n";
 	$fh = fopen($logfile, 'a');
 	fwrite($fh, $log_line);
 	fclose($fh);
}

if ( isset($_REQUEST['action']) && $_REQUEST['action'] == 'generateDiscountCode' ) {

	$discountCode = isset($_REQUEST['wfserial']) ? $_REQUEST['wfserial'] : '' ;
	$url = 'https://CARTNAME.cartloom.com/api/discounts/add';
	$apiKey = 'SPECIFY-YOUR-CARTLOOM-API-KEY-HERE' ;
	$data = array(
				'enabled' => '1', // "1" is true, "0" is false
				'auto' => '0', // "1" is true, "0" is false
				'type' => 'percent', // Change this to "fixed" if you want to set a fixed amount rather than a percentage
				'unlimited' => '0', // "1" is true, "0" is false
				'amount' => '30.00', // The discount amount, make sure the "type" of discount is what you want it to be!
				'target' => 'all', // Can also be set to "product" or "total"
				'start_date' => '2015-02-01', // Set the start date
				'stop_date' => '2015-12-31', // Set the end date
				'code' => $discountCode,
				'title' => 'Discount for Waterfall customer',
				'allowance' => '1' // This limits the discount to a single use
			);

	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n" .
	        			 "X-API-KEY: " . $apiKey, //Specify your own Cartloom API key here
	        'method'  => 'POST',
	        'content' => http_build_query($data),
	    ),
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	$result = json_decode($result, true);

	if ( $result['result'] == 'success' ) {
		log_line('-- The following Waterfall discount code was successfully created: ' . $discountCode);
		echo "success";
	} else {
		log_line('-- Cartloom did not return a successful reply when attempting to create the following discount code: ' . $discountCode);
		echo "failure";
	}

} else {
	log_line('URL request was missing a required parameter. Contact NimbleHost for assistance.');
}

?>